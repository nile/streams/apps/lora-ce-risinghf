# LoRa RisingHF

## Overview

### Owners & Administrators

| Title       | Details                                                                              |
|-------------|--------------------------------------------------------------------------------------|
| **E-group** | [BE-it-temp-admin](https://groups-portal.web.cern.ch/group/BE-it-temp-admin/details) |
| **People**  | [Lukas Goralcyk](https://phonebook.cern.ch/search?q=Lukas+Goralcyk)                  |

### Kafka Topics

| Environment    | Topic Name                                                                                                  |
|----------------|-------------------------------------------------------------------------------------------------------------|
| **Production** | [lora-risinghf](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-risinghf)                 |
| **Production** | [lora-risinghf-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/gp3/all-topics/lora-risinghf-decoded) |
| **QA**         | [lora-risinghf](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-risinghf)                 |
| **QA**         | [lora-risinghf-decoded](https://nile-kafka-ui.app.cern.ch/ui/clusters/qa3/all-topics/lora-risinghf-decoded) |

### Configuration

| Title                        | Details                                                                                    |
|------------------------------|--------------------------------------------------------------------------------------------|
| **Application Name**         | BE-it-temp                                                                                 |
| **Configuration Repository** | [app-configs/lora-risinghf](https://gitlab.cern.ch/nile/streams/app-configs/lora-risinghf) |
